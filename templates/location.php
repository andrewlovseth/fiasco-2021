<?php

/*

	Template Name: Location

*/

get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="location grid">
			<?php get_template_part('template-parts/global/page-header'); ?>

			<?php get_template_part('templates/location/info'); ?>

			<?php get_template_part('templates/location/exterior-photo'); ?>

			<?php get_template_part('templates/location/map'); ?>
		</section>

	    <?php get_template_part('templates/home/construction'); ?>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>