<?php
    $home = get_option('page_on_front');
    $construction = get_field('construction', $home);
    $show = $construction['show'];
    $headline = $construction['headline'];
    $copy = $construction['copy'];
    $image = $construction['image'];

    if($show):

?>

    <section class="construction grid">
        <div class="construction__info">
            <h3 class="construction__headline"><?php echo $headline; ?></h3>

            <div class="copy construction__copy">
                <?php echo $copy; ?>
            </div>
        </div>

        <div class="construction__image">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>

        </div>
    </section>

<?php endif; ?>