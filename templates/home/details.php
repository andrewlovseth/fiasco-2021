<section class="details grid">
    <div class="info">
        <div class="headline">
            <h2><?php echo get_field('headline'); ?></h2>
        </div>

        <div class="copy">
            <?php echo get_field('description'); ?>
        </div>				
    </div>
    
    <div class="gallery">
        <div class="photo inset">
            <div class="content">
                <img src="<?php $image = get_field('inset_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>

        <div class="photo">
            <div class="content">
                <img src="<?php $image = get_field('location_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>

        <div class="photo">
            <div class="content">
                <img src="<?php $image = get_field('hours_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>
    </div>


    
</section>