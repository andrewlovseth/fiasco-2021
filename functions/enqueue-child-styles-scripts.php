<?php
// Enqueue custom styles and scripts
function bearsmith_enqueue_child_styles_and_scripts() {
    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'fiasco-style', $dir . '/fiasco.css', '', '' );
    wp_enqueue_script( 'fiasco-scripts', $dir . '/js/fiasco.js', '', '', true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_child_styles_and_scripts', 11);